//
//  SettingsViewController.m
//  SPATime-N
//
//  Created by David Weber on 3/2/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Settings";
        // Set Tab Bar Item
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"157-wrench"] tag:0];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSett:)];
    
    _inputCUrl.delegate = self;
    _inputCUrl.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"curl"];
    
    _activityIndicatorS = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorS];
    _activityIndicatorS.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark_hub"]];
    _activityIndicatorS.mode = MBProgressHUDModeCustomView;
    _activityIndicatorS.labelText = @"Success";
}

-(void)saveSett:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:_inputCUrl.text forKey:@"curl"];
    [_activityIndicatorS show:YES];
    [_activityIndicatorS hide:YES afterDelay:3];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
