//
//  ItemsViewController.m
//  SPATime-N
//
//  Created by David Weber on 2/26/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "ItemsViewController.h"
#import "LoginViewController.h"
#import "ItemViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ItemsViewController ()

@property NSMutableArray *timeEntries;
@property NSString *selDate;

@end

@implementation ItemsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Set Title
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd.MM.YYYY"];
        self.title = [dateFormat stringFromDate:[NSDate date]];
        // Set Tab Bar Item
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Overview" image:[UIImage imageNamed:@"83-calendar"] tag:0];
        // Set Badge Value
        //[self.tabBarItem setBadgeValue:@"Michele"];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editItems:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Date" style:UIBarButtonItemStyleBordered target:self action:@selector(selectDate:)];
    
    // Activity Indicator
    _activityIndicatorL = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:_activityIndicatorL];
    _activityIndicatorL.mode = MBProgressHUDModeIndeterminate;
    _activityIndicatorL.labelText = @"Loading";
    
    _activityIndicatorS = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:_activityIndicatorS];
    _activityIndicatorS.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark_hub"]];
    _activityIndicatorS.mode = MBProgressHUDModeCustomView;
    _activityIndicatorS.labelText = @"Success";
    
    _activityIndicatorF = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:_activityIndicatorF];
    _activityIndicatorF.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"caution_hub"]];
    _activityIndicatorF.mode = MBProgressHUDModeCustomView;
    _activityIndicatorF.labelText = @"Fail";
    
    // Table BG
    self.tableView.backgroundColor = [[UIColor scrollViewTexturedBackgroundColor] colorWithAlphaComponent:1];
    
    // GET Time
    [self requestLoad];
    
}

- (void)requestLoad{
    [_activityIndicatorL show:YES];
    _request = [AjaxRequest alloc];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    _selDate = [dateFormat stringFromDate:[NSDate date]];
    [_request getTimesWithDate:_selDate callback:^(NSData * text){
        [self loadTime:text];
        _request = nil;
    } onError:^(NSError * error){
        _request = nil;
        [_activityIndicatorL hide:YES];
        
        _activityIndicatorF.labelText = @"Network error...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }];
}


- (BOOL)loadTime:(id)text{
    
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:text options:0 error:nil];
    
    if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ss-error"]) {
        [_activityIndicatorL hide:YES];
        LoginViewController *loginView = [[LoginViewController alloc] init];
        loginView.refView = self;
        self.modalPresentationStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:loginView animated:YES completion:nil];
        return YES;
    }
    
    _timeEntries = [NSMutableArray array];
    
    for (NSDictionary * dic in jsonData){
        [_timeEntries addObject:[[TimeEntry alloc] initWithDictionary:dic]];
    }
    
    [_activityIndicatorL hide:YES];
    [self.tableView reloadData];
    return YES;
}

- (void)editItems:(id)sender {
    [self.tableView setEditing:![self.tableView isEditing] animated:YES];
}

- (void)selectDate:(id)sender {
    
    _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [_actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    //CGRect pickerFrame = CGRectMake(0, 45, 0, 0);
    _datePickerMain = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 320, 450)];
    _datePickerMain.datePickerMode = UIDatePickerModeDate;
    
    [_actionSheet addSubview:_datePickerMain];
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Set"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [_actionSheet addSubview:closeButton];
    [_actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [_actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
}

-(void)dismissActionSheet:(id)sender{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd.MM.YYYY"];
    self.title = [dateFormat stringFromDate:_datePickerMain.date];
    NSLog(@"Main Date Picked: %@" , [df stringFromDate:_datePickerMain.date]);
    
    [_actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    _selDate = [df stringFromDate:_datePickerMain.date];
    
    [_activityIndicatorL show:YES];
    
    _request = [AjaxRequest alloc];
    [_request getTimesWithDate:_selDate callback:^(NSData * text){
        [self loadTime:text];
        _request = nil;
    } onError:^(NSError * error){
        _request = nil;
        [_activityIndicatorL hide:YES];
        
        _activityIndicatorF.labelText = @"Network error...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return _timeEntries.count;
}

- (void)reloadTime{
    [_activityIndicatorL show:YES];
    
    _request = [AjaxRequest alloc];
    [_request getTimesWithDate:_selDate callback:^(NSData * text){
        [self loadTime:text];
        _request = nil;
    } onError:^(NSError * error){
        _request = nil;
        [_activityIndicatorL hide:YES];
        
        _activityIndicatorF.labelText = @"Network error...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }];
}


#define MAINLABEL_TAG 1
#define SECONDLABEL_TAG 2
#define DATELABEL_TAG 3
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifierMain";
    //[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Fetch Books
    //TimeEntry *TimeEntry = [_timeEntries objectAtIndex:[indexPath row]];
    // Configure Cell
    //[cell.textLabel setText:TimeEntry.pspnr];
    
    
    UILabel *mainLabel, *secondLabel, *dateLabel;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        
        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 4.0, 220.0, 15.0)];
        mainLabel.tag = MAINLABEL_TAG;
        mainLabel.font = [UIFont systemFontOfSize:17.0];
        mainLabel.textColor = [UIColor whiteColor];
        mainLabel.backgroundColor = [[UIColor scrollViewTexturedBackgroundColor] colorWithAlphaComponent:1];
        //mainLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        mainLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        mainLabel.highlightedTextColor = [UIColor whiteColor];
        [cell.contentView addSubview:mainLabel];
        
        secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 25.0, 220.0, 15.0)];
        secondLabel.tag = SECONDLABEL_TAG;
        secondLabel.font = [UIFont systemFontOfSize:12.0];
        secondLabel.textColor = [UIColor lightTextColor];
        secondLabel.backgroundColor = [[UIColor scrollViewTexturedBackgroundColor] colorWithAlphaComponent:1];
        //secondLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        secondLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        secondLabel.highlightedTextColor = [UIColor whiteColor];
        [cell.contentView addSubview:secondLabel];
        
        dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(230.0, 4.0, 220.0, 15.0)];
        dateLabel.tag = DATELABEL_TAG;
        dateLabel.font = [UIFont systemFontOfSize:12.0];
        dateLabel.textColor = [UIColor lightTextColor];
        dateLabel.backgroundColor = [[UIColor scrollViewTexturedBackgroundColor] colorWithAlphaComponent:1];
        //dateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        dateLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        dateLabel.highlightedTextColor = [UIColor whiteColor];
        [cell.contentView addSubview:dateLabel];
    } else {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
        dateLabel = (UILabel *)[cell.contentView viewWithTag:DATELABEL_TAG];
    }
    TimeEntry *timeEntry = [_timeEntries objectAtIndex:indexPath.row];
    mainLabel.text = timeEntry.pspnr;
    if (timeEntry.memo.length < 15) {
        secondLabel.text = timeEntry.memo;
    } else {
        secondLabel.text = [NSString stringWithFormat:@"%@", [timeEntry.memo substringWithRange:NSMakeRange(0, 15)]];
    }
    
    dateLabel.text = [NSString stringWithFormat:@"%@:%@ bis %@:%@",
                    [timeEntry.timefrom substringWithRange:NSMakeRange(0, 2)], [timeEntry.timefrom substringWithRange:NSMakeRange(2, 2)],
                    [timeEntry.timeto substringWithRange:NSMakeRange(0, 2)], [timeEntry.timeto substringWithRange:NSMakeRange(2, 2)]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        TimeEntry *timeEntry = [_timeEntries objectAtIndex:indexPath.row];
        [_activityIndicatorL show:YES];
        _request = [AjaxRequest alloc];
        [_request deleteTime:timeEntry.pri callback:^(NSData * text){
            _request = nil;
            [_activityIndicatorL hide:YES];
            NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:text options:0 error:nil];
            
            if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ss-error"]) {
                LoginViewController *loginView = [[LoginViewController alloc] init];
                loginView.refView = self;
                self.modalPresentationStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:loginView animated:YES completion:nil];
            } else if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ap-success"]){
                [_activityIndicatorL show:YES];
                _request = [AjaxRequest alloc];
                [_request getTimesWithDate:_selDate callback:^(NSData * text){
                    [self loadTime:text];
                    _request = nil;
                } onError:^(NSError * error){
                    _request = nil;
                    [_activityIndicatorL hide:YES];
                    
                    _activityIndicatorF.labelText = @"Network error...";
                    [_activityIndicatorF show:YES];
                    [_activityIndicatorF hide:YES afterDelay:3];
                }];
            } else {
                _activityIndicatorF.labelText = @"Network error...";
                [_activityIndicatorF show:YES];
                [_activityIndicatorF hide:YES afterDelay:3];
            }
            
        } onError:^(NSError * error){
            _request = nil;
            [_activityIndicatorL hide:YES];
            
            _activityIndicatorF.labelText = @"Network error...";
            [_activityIndicatorF show:YES];
            [_activityIndicatorF hide:YES afterDelay:3];
        }];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemViewController *itemViewConrtoller = [[ItemViewController alloc] initWithNibName:@"ItemViewController" bundle:nil];
    [itemViewConrtoller setTimeEntry:[_timeEntries objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:itemViewConrtoller animated:YES];
}

@end
