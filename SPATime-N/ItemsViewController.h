//
//  ItemsViewController.h
//  SPATime-N
//
//  Created by David Weber on 2/26/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#import "AjaxRequest.h"
#import "TimeEntry.h"

@interface ItemsViewController : UITableViewController

@property UIActionSheet *actionSheet;
@property UIDatePicker *datePickerMain;

@property MBProgressHUD *activityIndicatorL;
@property MBProgressHUD *activityIndicatorS;
@property MBProgressHUD *activityIndicatorF;

// Ajax
@property AjaxRequest *request;
- (void)reloadTime;

@end
