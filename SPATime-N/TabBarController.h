//
//  TabBarController.h
//  SPATime-N
//
//  Created by David Weber on 2/28/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TabBarController : NSObject <UITabBarControllerDelegate>

@end
