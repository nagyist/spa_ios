//
//  ItemViewController.h
//  SPATime-N
//
//  Created by David Weber on 2/28/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeEntry.h"
#import "MBProgressHUD.h"
#import "AjaxRequest.h"

@interface ItemViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *inputPSP;
@property (weak, nonatomic) IBOutlet UITextField *inputDate;
@property (weak, nonatomic) IBOutlet UITextField *inputTimeF;
@property (weak, nonatomic) IBOutlet UITextField *inputTimeT;
@property (weak, nonatomic) IBOutlet UITextView *inputMemo;

@property (weak, nonatomic) IBOutlet UILabel *lblPri;
@property (weak, nonatomic) IBOutlet UITextField *inputBname;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollEdit;


@property TimeEntry *timeEntry;

@property UIDatePicker *datePicker;
@property UIDatePicker *timeTPicker;
@property UIDatePicker *timeFPicker;

@property MBProgressHUD *activityIndicatorL;
@property MBProgressHUD *activityIndicatorS;
@property MBProgressHUD *activityIndicatorF;

@property (strong, nonatomic) IBOutlet UITableView *tblAutocomplete;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollAutocomplete;
@property NSMutableArray *autoCompleteArray;
@property UITextView *activeTextView;

// Ajax
@property AjaxRequest *request;

@end
