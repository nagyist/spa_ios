//
//  SettingsViewController.h
//  SPATime-N
//
//  Created by David Weber on 3/2/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface SettingsViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *inputCUrl;

@property MBProgressHUD *activityIndicatorS;

@end
