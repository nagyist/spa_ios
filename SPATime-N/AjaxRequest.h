//
//  AjaxRequest.h
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <Foundation/Foundation.h>
#define baselURL @"http://riagsrv10.riagdom2.resource.ch:8010/zspa_mobile/"
//#define baselURL @"http://w3tec.ch/testarea/spa/"

@interface AjaxRequest : NSObject

- (void)getTimesWithDate:(NSString *)date callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;
- (void)getPSPs:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;
- (void)postTime:(NSDictionary *)data callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;
- (void)updateTime:(NSDictionary *)data andID:(int)pri callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;
- (void)getAuth:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;
- (void)deleteTime:(int)data callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock;

@end
