//
//  AjaxRequest.m
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "AjaxRequest.h"

@interface AjaxRequest ()
    @property NSURLConnection *connection;
    @property (nonatomic, copy) void (^block)(NSData *);
    @property (nonatomic, copy) void (^failBlock)(NSError *);
@end


@implementation AjaxRequest

- (NSString *)getBaseUrl{
    NSString *cUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"curl"];
    return (cUrl.length ? cUrl : baselURL);
}

- (void)getTimesWithDate:(NSString *)date callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@?date=%@", [self getBaseUrl], @"time", date];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"GET"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)getPSPs:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@", [self getBaseUrl], @"psp"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"GET"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)getAuth:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@", [self getBaseUrl], @"auth"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"GET"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)postTime:(NSDictionary *)data callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@", [self getBaseUrl], @"time"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *myRequestData = [self encodeDictionary:data];
    
    [request setHTTPBody:myRequestData];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)updateTime:(NSDictionary *)data andID:(int)pri callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@/%d", [self getBaseUrl], @"time", pri];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *myRequestData = [self encodeDictionary:data];
    
    [request setHTTPBody:myRequestData];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)deleteTime:(int)data callback:(void (^)(NSData *))block onError:(void (^)(NSError *))failBlock{
    // Save block to local property
    self.block = block;
    self.failBlock = failBlock;
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@/%d", [self getBaseUrl], @"time", data];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:getUrl]];
    
    [request setHTTPMethod:@"DELETE"];
    
    //Authentication
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"username"] forHTTPHeaderField:@"X-Sapu"];
    [request setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"] forHTTPHeaderField:@"X-Sapp"];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSAssert(self.connection != nil, @"Failure to create URL connection.");
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    NSLog(@"habe daten: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    // send data back to block
    self.block(data);
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    int statusCode = [((NSHTTPURLResponse *)response) statusCode];
    if (statusCode >= 400) {
        [connection cancel];  // stop connecting; no more delegate messages
        NSDictionary *errorInfo
        = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:
                                              NSLocalizedString(@"Server returned status code %d",@""),
                                              statusCode]
                                      forKey:NSLocalizedDescriptionKey];
        
        
        NSError *statusError = [NSError errorWithDomain:[self getBaseUrl]
                                code:statusCode
                                userInfo:errorInfo];
        [self connection:connection didFailWithError:statusError];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"didFail");
    self.failBlock(error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"DidFinishLoading");
}

@end
