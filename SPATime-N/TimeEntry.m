//
//  TimeEntry.m
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "TimeEntry.h"

@implementation TimeEntry

- (id)initWithDictionary:(NSDictionary *)dict{
    self.mandt = [dict objectForKey:@"MANDT"];
    self.pri = [[dict objectForKey:@"ID"] integerValue];
    self.pspnr = [dict objectForKey:@"PSPNR"];
    self.bname = [dict objectForKey:@"BNAME"];
    self.atdate = [dict objectForKey:@"ATDATE"];
    self.timefrom = [dict objectForKey:@"TIMEFROM"];
    self.timeto = [dict objectForKey:@"TIMETO"];
    self.memo = [dict objectForKey:@"MEMO"];
    return self;
}

@end
