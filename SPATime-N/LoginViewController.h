//
//  LoginViewController.h
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AjaxRequest.h"

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textUsername;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UISwitch *switchLogin;

@property AjaxRequest *request;

- (IBAction)loginClicked:(id)sender;

@property MBProgressHUD *activityIndicatorL;
@property MBProgressHUD *activityIndicatorS;
@property MBProgressHUD *activityIndicatorF;

@property id refView;

@end
