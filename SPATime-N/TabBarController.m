//
//  TabBarController.m
//  SPATime-N
//
//  Created by David Weber on 2/28/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "TabBarController.h"

@implementation TabBarController

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    UINavigationController *navController = (UINavigationController *)viewController;
    NSArray *viewControllers = [navController childViewControllers];
    UIViewController *currViewController = [viewControllers objectAtIndex:0];
    
    if ([currViewController respondsToSelector:@selector(requestLoad)]){
        [currViewController performSelector:@selector(requestLoad)];
    }
    
}

@end
