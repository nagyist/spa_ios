//
//  LoginViewController.m
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Login";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // set login data
    NSString *result = [[NSUserDefaults standardUserDefaults] stringForKey:@"save"];
    if (result != nil) {
        _textUsername.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        _textPassword.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"password"];
    } else {
        [_switchLogin setOn:NO];
    }
    
    // Activity Indicator
    _activityIndicatorL = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorL];
    _activityIndicatorL.mode = MBProgressHUDModeIndeterminate;
    _activityIndicatorL.labelText = @"Loading";
    
    _activityIndicatorS = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorS];
    _activityIndicatorS.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark_hub"]];
    _activityIndicatorS.mode = MBProgressHUDModeCustomView;
    _activityIndicatorS.labelText = @"Success";
    
    _activityIndicatorF = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorF];
    _activityIndicatorF.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"caution_hub"]];
    _activityIndicatorF.mode = MBProgressHUDModeCustomView;
    _activityIndicatorF.labelText = @"Fail";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginClicked:(id)sender{
    NSLog(@"save: %i", _switchLogin.isOn);
    
    if(_switchLogin.isOn == 1){
        [[NSUserDefaults standardUserDefaults] setObject:_textUsername.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:_textPassword.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"save"];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:_textUsername.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:_textPassword.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"save"];
    }
    
    [_activityIndicatorL show:YES];
    _request = [AjaxRequest alloc];
    [_request getAuth:^(NSData * text){
        [self getAuth:text];
        _request = nil;
    } onError:^(NSError * error){
        _request = nil;
        [_activityIndicatorL hide:YES];
        
        _activityIndicatorF.labelText = @"Network error...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }];
    
}

- (BOOL)getAuth:(id)text{
    
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:text options:0 error:nil];
    
    if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ss-login"]) {
        [_activityIndicatorL hide:YES];
        [_refView performSelector:@selector(requestLoad)];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        _activityIndicatorF.labelText = @"Login incorrect";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }
    return YES;
}




@end
