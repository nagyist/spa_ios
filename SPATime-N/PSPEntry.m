//
//  PSPEntry.m
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "PSPEntry.h"


@implementation PSPEntry

- (id)initWithDictionary:(NSDictionary *)dict{
    self.mandt = [dict objectForKey:@"MANDT"];
    self.pspnr = [dict objectForKey:@"PSPNR"];
    self.name = [dict objectForKey:@"NAME"];
    self.memo = [dict objectForKey:@"MEMO"];
    self.stati = [dict objectForKey:@"STATI"];
    return self;
}

@end
