//
//  AppDelegate.h
//  SPATime-N
//
//  Created by David Weber on 2/26/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TabBarController *tabBarController;

@end
