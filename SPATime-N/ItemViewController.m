//
//  ItemViewController.m
//  SPATime-N
//
//  Created by David Weber on 2/28/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import "ItemViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LoginViewController.h"
#import "PSPEntry.h"

@interface ItemViewController ()
@property NSMutableArray *pspEntries;
@end

@implementation ItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Edit";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set Button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveEntry:)];
    
    // Set Labels
    _inputPSP.text = _timeEntry.pspnr;
    _lblPri.text = [NSString stringWithFormat:@"%d", _timeEntry.pri];
    _inputBname.text = _timeEntry.bname;
    _inputDate.text =
        [NSString stringWithFormat:@"%@-%@-%@",
            [_timeEntry.atdate substringWithRange:NSMakeRange(4, 4)],
            [_timeEntry.atdate substringWithRange:NSMakeRange(2, 2)],
            [_timeEntry.atdate substringWithRange:NSMakeRange(0, 2)]
         ];
    _inputTimeF.text = [NSString stringWithFormat:@"%@:%@:%@",
                            [_timeEntry.timefrom substringWithRange:NSMakeRange(0, 2)],
                            [_timeEntry.timefrom substringWithRange:NSMakeRange(2, 2)],
                            [_timeEntry.timefrom substringWithRange:NSMakeRange(4, 2)]
                       ];
    _inputTimeT.text =  [NSString stringWithFormat:@"%@:%@:%@",
                            [_timeEntry.timeto substringWithRange:NSMakeRange(0, 2)],
                            [_timeEntry.timeto substringWithRange:NSMakeRange(2, 2)],
                            [_timeEntry.timeto substringWithRange:NSMakeRange(4, 2)]
                        ];
    _inputMemo.text = _timeEntry.memo;
    
    // Draw input for memo
    UITextField* roundRect = [[UITextField alloc] initWithFrame:CGRectMake(20, 225, 280, 140)];
    [roundRect setBorderStyle:UITextBorderStyleRoundedRect];
    roundRect.enabled = NO;
    [self.scrollEdit addSubview:roundRect];
    [self.scrollEdit addSubview:self.inputMemo];
    
    // Toolbar for Keyboard Memo
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboardMemo)];
    NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
    [toolbar setItems:itemsArray];
    [_inputMemo setInputAccessoryView:toolbar];
    
    // datepicker
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_inputDate setInputView:_datePicker];
    NSDateFormatter *parser = [[NSDateFormatter alloc] init];
    [parser setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [parser dateFromString:_inputDate.text];
    [_datePicker setDate:date];
    
    
    // Toolbar for Keyboard Datepicker
    UIToolbar *toolbarDate = [[UIToolbar alloc] init];
    [toolbarDate setBarStyle:UIBarStyleBlackTranslucent];
    [toolbarDate sizeToFit];
    UIBarButtonItem *flexButtonDate = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButtonDate =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboardDate)];
    NSArray *itemsArrayDate = [NSArray arrayWithObjects:flexButtonDate, doneButtonDate, nil];
    [toolbarDate setItems:itemsArrayDate];
    [_inputDate setInputAccessoryView:toolbarDate];
    
    
    // datepicker for Time To
    _timeTPicker = [[UIDatePicker alloc] init];
    _timeTPicker.datePickerMode = UIDatePickerModeTime;
    _timeTPicker.minuteInterval = 15;
    _timeTPicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"de_CH"];
    [_timeTPicker addTarget:self action:@selector(datePickerValueChangedT:) forControlEvents:UIControlEventValueChanged];
    [_inputTimeT setInputView:_timeTPicker];
    [parser setDateFormat:@"HH:mm:ss"];
    date = [parser dateFromString:_inputTimeT.text];
    [_timeTPicker setDate:date];
    
    // Toolbar for Keyboard Datepicker
    UIToolbar *toolbarTimeT = [[UIToolbar alloc] init];
    [toolbarTimeT setBarStyle:UIBarStyleBlackTranslucent];
    [toolbarTimeT sizeToFit];
    UIBarButtonItem *flexButtonTimeT = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButtonTimeT =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboardTimeT)];
    NSArray *itemsArrayTimeT = [NSArray arrayWithObjects:flexButtonTimeT, doneButtonTimeT, nil];
    [toolbarTimeT setItems:itemsArrayTimeT];
    [_inputTimeT setInputAccessoryView:toolbarTimeT];
    
    
    // datepicker for Time From
    _timeFPicker = [[UIDatePicker alloc] init];
    _timeFPicker.datePickerMode = UIDatePickerModeTime;
    _timeFPicker.minuteInterval = 15;
    _timeFPicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"de_CH"];
    [_timeFPicker addTarget:self action:@selector(datePickerValueChangedF:) forControlEvents:UIControlEventValueChanged];
    [_inputTimeF setInputView:_timeFPicker];
    [parser setDateFormat:@"HH:mm:ss"];
    date = [parser dateFromString:_inputTimeF.text];
    [_timeFPicker setDate:date];
    
    // Toolbar for Keyboard Datepicker
    UIToolbar *toolbarTimeF = [[UIToolbar alloc] init];
    [toolbarTimeF setBarStyle:UIBarStyleBlackTranslucent];
    [toolbarTimeF sizeToFit];
    UIBarButtonItem *flexButtonTimeF = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButtonTimeF =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboardTimeF)];
    NSArray *itemsArrayTimeF = [NSArray arrayWithObjects:flexButtonTimeF, doneButtonTimeF, nil];
    [toolbarTimeF setItems:itemsArrayTimeF];
    [_inputTimeF setInputAccessoryView:toolbarTimeF];
    
    
    // Activity Indicator
    _activityIndicatorL = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorL];
    _activityIndicatorL.mode = MBProgressHUDModeIndeterminate;
    _activityIndicatorL.labelText = @"Loading";
    
    _activityIndicatorS = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorS];
    _activityIndicatorS.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark_hub"]];
    _activityIndicatorS.mode = MBProgressHUDModeCustomView;
    _activityIndicatorS.labelText = @"Success";
    
    _activityIndicatorF = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_activityIndicatorF];
    _activityIndicatorF.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"caution_hub"]];
    _activityIndicatorF.mode = MBProgressHUDModeCustomView;
    _activityIndicatorF.labelText = @"Fail";
    
    // PSP Autocomplete
    _inputPSP.autocorrectionType = UITextAutocorrectionTypeNo;
    
    _tblAutocomplete.delegate = self;
    _tblAutocomplete.dataSource = self;
    _tblAutocomplete.scrollEnabled = YES;
    
    _scrollAutocomplete.hidden = YES;
    _scrollAutocomplete.layer.borderWidth = 1;
    _scrollAutocomplete.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _scrollAutocomplete.layer.cornerRadius = 4;
    
    _autoCompleteArray = [[NSMutableArray alloc] init];
    
    // GET PSP
    [self requestLoad];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
        NSArray *viewControllers = [self.navigationController childViewControllers];
        
        for (UIViewController *currViewController in viewControllers) {
        
            if ([currViewController respondsToSelector:@selector(reloadTime)]){
                [currViewController performSelector:@selector(reloadTime)];
            }
            
        }
        
        
        
    }
    [super viewWillDisappear:animated];
}

-(void)requestLoad{
    [_activityIndicatorL show:YES];
    _request = [AjaxRequest alloc];
    [_request getPSPs:^(NSData * text){
        [self loadPSP:text];
        _request = nil;
    } onError:^(NSError * error){
        _request = nil;
        [_activityIndicatorL hide:YES];
        
        _activityIndicatorF.labelText = @"Network error...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }];
}

- (BOOL)loadPSP:(id)text{
    NSLog(@"daten loadPSP: %@", text);
    
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:text options:0 error:nil];
    
    if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ss-error"]) {
        [_activityIndicatorL hide:YES];
        LoginViewController *loginView = [[LoginViewController alloc] init];
        self.modalPresentationStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:loginView animated:YES completion:nil];
        return YES;
    }
    
    _pspEntries = [NSMutableArray array];
    
    for (NSDictionary * dic in jsonData){
        [_pspEntries addObject:[[PSPEntry alloc] initWithDictionary:dic]];
    }
    
    [_activityIndicatorL hide:YES];
    return YES;
}


-(void)saveEntry:(id)sender{
    
    // Check input
    if (_inputPSP.text.length > 0 && _inputDate.text.length > 0 &&
        _inputTimeF.text.length > 0 && _inputTimeT.text.length >0 &&
        _inputMemo.text.length > 0) {
        
        // dic fill
        NSDictionary *dictData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _inputPSP.text, @"search-psp",
                                  _inputDate.text, @"date",
                                  [_inputTimeF.text stringByReplacingOccurrencesOfString:@":" withString:@""], @"time",
                                  [_inputTimeT.text stringByReplacingOccurrencesOfString:@":" withString:@""], @"timet",
                                  _inputMemo.text, @"textarea-des",
                                  nil];
        
        // request
        [_activityIndicatorL show:YES];
        _request = [AjaxRequest alloc];
        [_request updateTime:dictData andID:_timeEntry.pri callback:^(NSData * text){
            [_activityIndicatorL hide:YES];
            [self updateTime:text];
            _request = nil;
        } onError:^(NSError * error){
            _request = nil;
            [_activityIndicatorL hide:YES];
            
            _activityIndicatorF.labelText = @"Network error...";
            [_activityIndicatorF show:YES];
            [_activityIndicatorF hide:YES afterDelay:3];
        }];
        
    } else {
        _activityIndicatorF.labelText = @"Input missing...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }
    
    
}

- (BOOL)updateTime:(id)text{
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:text options:0 error:nil];
    
    if ([[[jsonData objectAtIndex:0] objectForKey:@"res"] isEqual: @"ap-success"]) {
        [_activityIndicatorS show:YES];
        [_activityIndicatorS hide:YES afterDelay:3];
    } else {
        _activityIndicatorF.labelText = @"Input incorecct...";
        [_activityIndicatorF show:YES];
        [_activityIndicatorF hide:YES afterDelay:3];
    }
    
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    if(substring.length != 0){
        [self searchAutocompleteEntriesWithSubstring:substring];
    } else {
        _scrollAutocomplete.hidden = YES;
    }
    return YES;
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autoCompleteArray
    // The items in this array is what will show up in the table view
    
    [_autoCompleteArray removeAllObjects];
    
    for(PSPEntry *entry in _pspEntries) {
        
        NSRange substringRangeLowerCase = [[entry.pspnr lowercaseString] rangeOfString: [substring lowercaseString]];
        
        if (substringRangeLowerCase.length != 0 ){
            [_autoCompleteArray addObject:entry.pspnr];
        }
    }
    
    _scrollAutocomplete.hidden = NO;
    [_tblAutocomplete reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    
    //Resize auto complete table based on how many elements will be displayed in the table
    if (_autoCompleteArray.count >=3) {
        //_tblAutocomplete.frame = CGRectMake(20, 50, 280, _tblAutocomplete.frame.size.height*3);
        return _autoCompleteArray.count;
    }
    
    else if (_autoCompleteArray.count == 2) {
        //_tblAutocomplete.frame = CGRectMake(20, 50, 280, _tblAutocomplete.frame.size.height*2);
        return _autoCompleteArray.count;
    }
    
    else {
        //_tblAutocomplete.frame = CGRectMake(20, 50, 280, _tblAutocomplete.frame.size.height);
        return _autoCompleteArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    cell.textLabel.text = [_autoCompleteArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    _inputPSP.text = selectedCell.textLabel.text;
    [self finishedSearching];
}

- (void) finishedSearching {
    [_inputPSP resignFirstResponder];
    _scrollAutocomplete.hidden = YES;
}



-(void)datePickerValueChanged:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [self.inputDate setText:[df stringFromDate:_datePicker.date]];
}

-(void)datePickerValueChangedF:(id)sender{
    NSDateFormatter *hf = [[NSDateFormatter alloc] init];
    [hf setDateFormat:@"HH:mm:ss"];
    [self.inputTimeF setText:[hf stringFromDate:_timeFPicker.date]];
}

-(void)datePickerValueChangedT:(id)sender{
    NSDateFormatter *hf = [[NSDateFormatter alloc] init];
    [hf setDateFormat:@"HH:mm:ss"];
    [self.inputTimeT setText:[hf stringFromDate:_timeTPicker.date]];
}

-(void)resignKeyboardMemo{[_inputMemo resignFirstResponder];}
-(void)resignKeyboardDate{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [self.inputDate setText:[df stringFromDate:_datePicker.date]];
    [_inputDate resignFirstResponder];
}
-(void)resignKeyboardTimeT{
    NSDateFormatter *hf = [[NSDateFormatter alloc] init];
    [hf setDateFormat:@"HH:mm:ss"];
    [self.inputTimeT setText:[hf stringFromDate:_timeTPicker.date]];
    [_inputTimeT resignFirstResponder];
}
-(void)resignKeyboardTimeF{
    NSDateFormatter *hf = [[NSDateFormatter alloc] init];
    [hf setDateFormat:@"HH:mm:ss"];
    [self.inputTimeF setText:[hf stringFromDate:_timeFPicker.date]];
    [_inputTimeF resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)editEntry{
    
}

@end
