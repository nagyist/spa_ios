//
//  TimeEntry.h
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeEntry : NSObject

@property NSString *mandt;
@property NSInteger pri;
@property NSString *pspnr;
@property NSString *bname;
@property NSString *atdate;
@property NSString *timefrom;
@property NSString *timeto;
@property NSString *memo;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
