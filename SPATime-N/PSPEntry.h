//
//  PSPEntry.h
//  SPATime-N
//
//  Created by David Weber on 2/27/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSPEntry : NSObject

@property NSString *mandt;
@property NSString *pspnr;
@property NSString *name;
@property NSString *memo;
@property NSString *stati;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
