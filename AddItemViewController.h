//
//  AddItemViewController.h
//  SPATime-N
//
//  Created by David Weber on 2/26/13.
//  Copyright (c) 2013 w3tec. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#import "AjaxRequest.h"

@interface AddItemViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollMain;

@property UIDatePicker *datePicker;
@property UIDatePicker *timeTPicker;
@property UIDatePicker *timeFPicker;

@property (weak, nonatomic) IBOutlet UITextField *textPSP;
@property (weak, nonatomic) IBOutlet UITextField *textDate;
@property (weak, nonatomic) IBOutlet UITextField *textTimeF;
@property (weak, nonatomic) IBOutlet UITextField *textTimeT;
@property (weak, nonatomic) IBOutlet UITextView *textMemo;

@property UITextView *activeTextView;

@property MBProgressHUD *activityIndicatorL;
@property MBProgressHUD *activityIndicatorS;
@property MBProgressHUD *activityIndicatorF;

@property (strong, nonatomic) IBOutlet UITableView *tblAutocomplete;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollAutocomplete;
@property NSMutableArray *autoCompleteArray;

// Ajax
@property AjaxRequest *request;
//- (void)saveData:(id)text;


@end
